## Introduction ##

Project structure decisions was made with the expectation of usage such stack of technologies for application implementation:

1.  frontend: React + Redux Saga;
2.  backend: NodeJS;
3.  database: Postgre or MongoDB.

## Application structure ##

In my point of view such kind of application should have monolith architecture (with staging implementation of different parts of application, for example application to be initially deployed may have only core functionality like authorization and one of the features implemented, and all other features may be added to the application continiously). Microservice architecture for this kind of application in my point of view would unnecessary because of overweight (simple backend structure should not be complicataned by separation of functionality for each item of data type for feature).

![Application architecture image](https://i.imgur.com/c0hXefI.png)

Because of high amount of relation between application's data, relational database would be better choice than a document-oriented bases.

## Database ##

Database may have such tables:

*  Users (```will have user credentials, role, achievements, orders```),
*  AchievementList (```connects users with achievements scored by them```),
*  Achievement (```should contain all achievement descriptions and conditions to take them```),
*  PromoList (```connects registered promos with orders```),
*  Promo (```contains promo description and conditions```),
*  OrderList (```connects order with user who made some service activities```),
*  Order (```contains information about service type, price for services, potential promo usage and status of order```),
*  Menus (```contains full amount of generated menus from kitchen and bar```),
*  Menu (```contains information about food and drinks provided by the platform```),
*  EquipmentList (```contains the list of equipment suggested by the platform```),
*  Equipment (```contains equipment item description, price and other```),
*  News (```contains news for the proposed blog feed```),
*  Invoices (```invoice should be generated after order completion and register user and services chosen by user```).

Application for frontend and backend connection may use http protocol, there is no real-time functionality to implement for application so there is no need to use websockets, and because of monolith architecture - there is no need to use backend component connection technoligies like rabbitMQ.