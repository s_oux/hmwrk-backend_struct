Root directory for all components of the application.

Each component may has it's own:

- routines (elements describing actions which could be done on components);
- sagas (functions processing actions of components);
- services (functions calling data from backend in order to actions);
- components (for decomposition of complexity);
- containers (for grouping each part of component to a single, entire component).