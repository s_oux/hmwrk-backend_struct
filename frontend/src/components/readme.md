This folder will contain components implementing UI elements general for the whole application, like:

- Modal component;
- Header component;
- Footer component;
- Side navigation menu component;
- Loader wrapper component;
- And other components which would be used in other application UI components.

Also folder should contain higher order components for Routing purposes (like Private route component, which would render next component only if user has the access by his role rights; or Public route => accessible for all kinds of users).