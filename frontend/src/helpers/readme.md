This folder will contain files, which would help the application to work in proper way.

That would be files like:
- ApiHelper - to create requests to the backend,
- ReducerHelper - to simplify and unify the method of reducers creation,
- DateFormatHelper - to have one source of implementation the date data type in application,
- And other functionality which would be helpful for the development, but have no realtion to the UI implementation.