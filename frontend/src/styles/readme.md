This directory will contain root .css styles for all app.

Purpose of these ```.css``` styling files is to reset default browser styles, create unified styling for control components of the whole application, define general styling items like uniform fonts and colours.